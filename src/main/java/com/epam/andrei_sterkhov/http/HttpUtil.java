package com.epam.andrei_sterkhov.http;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtil.class);
    private CloseableHttpResponse response;

    public String getCsrfToken(HttpGet httpGet) throws IOException {
        String csrfToken = null;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            this.response = httpclient.execute(httpGet);
            String regex = "csrfToken\":\"[^\"]++";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher;
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                String line;
                while (reader.ready()) {
                    line = reader.readLine();
                    matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        String result = matcher.group();
                        csrfToken = result.substring(result.lastIndexOf('\"') + 1);
                    }
                }
            }
        }
        LOGGER.info("csrfToken is {}", csrfToken);
        return csrfToken;
    }

    public String getElementValue(String headerName, String elementName) {
        String result;
        Header header = response.getFirstHeader(headerName);

        HeaderElement[] elements = header.getElements();
        result = Arrays.stream(elements).filter(element -> element.getName().equals(elementName))
                .findFirst()
                .map(HeaderElement::getValue)
                .orElse("");

        LOGGER.info("{} is {}", elementName, result);
        return result;
    }

    public String getCoordinatesFromYandexMap(String placeToFind, String csrfToken, String yandexuid) throws URISyntaxException {
        String result = null;

        URI uri = new URIBuilder().setScheme("https")
                .setHost("yandex.ru")
                .setPath("/maps/api/search")
                .setParameter("text", placeToFind)
                .setParameter("lang", "ru_RU")
                .setParameter("csrfToken", csrfToken)
                .build();

        HttpGet httpget = new HttpGet(uri);

        httpget.setHeader("Cookie", "yandexuid=".concat(yandexuid));

        try (CloseableHttpClient httpclient = HttpClients.createDefault();
             CloseableHttpResponse anotherResponse = httpclient.execute(httpget)) {

            HttpEntity entity = anotherResponse.getEntity();

            String line;
            String regex = "coordinates\":.*?(?=,\")";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher;

            if (entity != null) {
                InputStream instream = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                while (reader.ready()) {
                    line = reader.readLine();
                    matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        result = matcher.group().replace("\"", "");
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error("", e);
        }
        return result;
    }
}