package com.epam.andrei_sterkhov.http;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HttpExample {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpExample.class);

    public static void main(String[] args) throws URISyntaxException, IOException {
        // Task 1 Получаем csrfToken

        URI uri = new URIBuilder()
                .setScheme("https")
                .setHost("yandex.ru")
                .setPath("/maps/44/Izhevsk")
                .build();


        // Получаем get запрос из билдера
        HttpGet httpGet = new HttpGet(uri);

        HttpUtil util = new HttpUtil();

        String csrfToken = util.getCsrfToken(httpGet);

        // Task 2 Получаем yandexuid
        String yandexuid = util.getElementValue("Set-cookie", "yandexuid");

        // Task 3
        String place = "Москва Кремль";
        String cord = util.getCoordinatesFromYandexMap(place, csrfToken, yandexuid);
        LOGGER.info("{} has {}", place, cord);
    }
}
